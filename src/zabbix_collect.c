#include <stdio.h>
#include <getopt.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <gmodule.h>
#include <ctype.h>

#include <libsoup/soup.h>
#include <jansson.h>
#include <curl/curl.h>

#define HOST		"turiavb"
#define PORT		8080
#define NB_LIGNES	500
#define BUFFER_SIZE	(100*1024)
#define BUFFER_GROW	(10*1024)

static enum {
	method_noout = 0,
	method_file = 1,
	method_influx = 2
} Method_out = method_noout;

static gboolean		Stop_asked	= FALSE;
static GMainLoop*	Loop		= NULL;
static FILE*		File_out	= NULL;
static FILE*		Reject_out	= NULL;
static CURL*		Influx		= NULL;

typedef struct _meas_buffer_s {
	gchar*	buffer;
	gsize	pos;
	gsize	cap;
} meas_buffer_t;

typedef struct _lignes_s {
	gsize			nb;
	meas_buffer_t*	data;
} lignes_t;

static GList*		Lignes = NULL;

static const char* Base = NULL;
static const char* User = NULL;
static const char* Pass = NULL;
static const char* Retention = NULL;

static GString* Measurement = NULL;

static struct _filter_s {
	const char* component;
	const char* measurement;
}* Filters = NULL;

//static char* Type_str[] = { "float", "char", "log", "unsigned", "text" };

static int	Verbose_flag = 0;
static lignes_t* add_new_buffer_lignes();
static void free_buffer_lignes( GList* which );

static void send_to_influx( gboolean force )
{
	if( Verbose_flag ) {
		fprintf( stderr, "%s()\n", __func__ );
	}

	if( Stop_asked == TRUE ) return;

	CURLcode ret;
	if( curl_easy_setopt( Influx, CURLOPT_POST, 1L ) != CURLE_OK ) return;
	struct curl_slist *slist1 = NULL;
	slist1 = curl_slist_append(slist1, "Content-Type: application/text");
	(void)curl_easy_setopt( Influx, CURLOPT_HTTPHEADER, slist1 );

	GList* buffer = g_list_last( Lignes );
	while( buffer ) {
		lignes_t* ligne = (lignes_t*)buffer->data;
		if( !ligne ) {
			free_buffer_lignes( buffer );
			break;
		}
		meas_buffer_t* meas  = (meas_buffer_t*)ligne->data;
		if( !meas ) {
			free_buffer_lignes( buffer );
			break;
		}
		if( ligne->nb < NB_LIGNES && force == FALSE ) {
			break;
		}
		ret = curl_easy_setopt( Influx, CURLOPT_POSTFIELDSIZE, (long)meas->pos );
		if( ret != CURLE_OK ) break;
		ret = curl_easy_setopt( Influx, CURLOPT_POSTFIELDS, meas->buffer );
		if( ret != CURLE_OK ) break;
		ret = curl_easy_perform( Influx );
		if( ret != CURLE_OK ) break;
		long codep;
		ret = curl_easy_getinfo( Influx, CURLINFO_RESPONSE_CODE, &codep );
		if( ret == CURLE_OK && codep != 204L && force == FALSE ) {
			struct curl_header *xie;
			CURLHcode h = curl_easy_header( Influx, "X-Influxdb-Error", 0, CURLH_HEADER, -1, &xie );
			if( strstr( xie->value, "partial write:" ) ) {
				// Ignore partial write errors.
				free_buffer_lignes( buffer );
			} else {
				Lignes = g_list_prepend( Lignes, add_new_buffer_lignes() );
				break;
			}
		} else {
			free_buffer_lignes( buffer );
		}
		buffer = g_list_last( Lignes );
	}
	curl_slist_free_all( slist1 );
}

static void free_buffer_lignes( GList* which )
{
	if( Verbose_flag ) {
		fprintf( stderr, "%s()\n", __func__ );
	}
	lignes_t* ligne = (lignes_t*)(which->data);
	if( ligne ) {
		meas_buffer_t* meas = (meas_buffer_t*)ligne->data;
		if( meas ) {
			if( meas->buffer ) g_free( meas->buffer );
			meas->buffer = NULL;
			meas->cap = 0;
			meas->pos = 0;
			g_free( meas );
		}
	}
	ligne->nb = 0;
	ligne->data = NULL;
	Lignes = g_list_remove( Lignes, which );
}

static meas_buffer_t* add_new_meas_buffer( void )
{
	if( Verbose_flag ) {
		fprintf( stderr, "%s()\n", __func__ );
	}
	meas_buffer_t* tmp = g_malloc0( sizeof(meas_buffer_t) );
	if( tmp ) {
		tmp->buffer = g_malloc0( BUFFER_SIZE );
		if( tmp->buffer ) {
			tmp->cap = BUFFER_SIZE;
			tmp->pos = 0;
		} else {
			g_free( tmp );
			return NULL;
		}
	} else {
		return NULL;
	}
	return tmp;
}

static lignes_t* add_new_buffer_lignes( void )
{
	if( Verbose_flag ) {
		fprintf( stderr, "%s()\n", __func__ );
	}
	lignes_t* tmp = g_malloc0( sizeof( lignes_t ) );
	if( tmp ) {
		tmp->nb = 0;
		tmp->data = add_new_meas_buffer();
		if( tmp->data == NULL ) {
			fprintf( stderr, "Echec à la création du buffer de mesures." );
			exit( EXIT_FAILURE );
		}
	}
	return tmp;
}

static gchar* grow_meas_buffer( meas_buffer_t* meas )
{
	if( Verbose_flag ) {
		fprintf( stderr, "%s()\n", __func__ );
	}
	if( meas ) {
		gchar* tmp = meas->buffer;

		tmp = realloc( tmp, meas->cap + BUFFER_GROW );
		if( tmp ) {
			meas->cap  += BUFFER_GROW;
			meas->buffer = tmp;
		}
		return tmp;
	}
	return NULL;
}

static void lignes_insert( GString* meas )
{
	if( Verbose_flag ) {
		fprintf( stderr, "%s(%d chars)", __func__, meas->len );
	}
	gsize len = meas->len;
	GList* first = g_list_first(Lignes);
	if( first == NULL ) {
		Lignes = g_list_prepend( Lignes, add_new_buffer_lignes());
		first = g_list_first(Lignes);
	}
	lignes_t*	ligne = (lignes_t*)(first->data);
	if( ligne->data == NULL ) {
		ligne->data = add_new_meas_buffer();
	}
	// Vérifier si la NB_LIGNE ième est atteinte.
	if( ligne->nb == NB_LIGNES || g_list_length(Lignes) > 1 ) {
		if( Stop_asked == TRUE ) return;
		send_to_influx( FALSE );
		lignes_insert( meas );	// <= le send vide les listes
		return;
	}
	// Vérifier la place pour stocker le contenu
	meas_buffer_t* buf = ligne->data;
	if( buf->pos + len + 1 > buf->cap ) {
		if( grow_meas_buffer( buf ) == NULL ) return;
	}
	// compter la ligne
	// stocker le contenu de la ligne
	memcpy( &buf->buffer[buf->pos], meas->str, meas->len+1 );
	buf->pos += meas->len;
	ligne->nb++;
	if( Verbose_flag ) {
		fprintf( stderr, " N° %d\n", ligne->nb );
	}
}

static void store_measure( GString* meas )
{
	if( Method_out == method_file ) {
		if( Verbose_flag && File_out != stdout ) {
			fprintf( stderr, meas->str );
		}
		fwrite( meas->str, 1, meas->len, File_out );
	} else if( Method_out == method_influx ) {
		lignes_insert( meas );
	}
	(void)g_string_truncate( meas, 0 );
}

static char* normalize( const char* s )
{
	char* pt = (char *)s;
	pt = strrchr( pt, ':' );
	if( pt == NULL ) {
		pt = (char  *)s;	// Pas de ':' dans 's'.
	} else {
		pt++;
	}
	while( *pt == ' ' ) pt++;	// supprimer espace de tête.
	char* tmp = g_strdup( pt );
	char* ltmp = tmp;
	while( *pt ) {	// conversion en minuscule
		if( *pt == ' ' || *pt == '/' ) {
			*ltmp = '_';
			ltmp++;
		} else if( *pt == '%' ) {
			*ltmp = 'P';
			ltmp++;
		} else if( *pt == '_' ) {
			*ltmp = *pt;
			ltmp++;
		} else if( isalnum(*pt) ) {
			*ltmp = tolower( *pt );
			ltmp++;
		}
		*ltmp='\0';
		pt++;
	}
	pt = tmp;
	return tmp;
}

static void decode_measurement( json_t* json_line, const  char* measure )
{
	json_t* tags = json_object_get( json_line, "item_tags" );

	g_string_printf( Measurement, "%s,", measure );
	GData*		klist = NULL;
	gpointer	dlist;

	g_datalist_init( &klist );
	if( json_is_array( tags ) ) {
		for( int index = 0; index < json_array_size( tags ); index++ ) {
			json_t* item = json_array_get( tags, index );
			const char*	key = json_string_value( json_object_get( item, "tag" ) );
			guint		count = 1;
			gchar		key_uniq[128];
			if( (dlist = g_datalist_get_data( &klist, key )) != NULL ) {
				count = GPOINTER_TO_UINT( dlist );
				g_snprintf( key_uniq, sizeof(key_uniq), "%s_%d", key, count );
				count++;
			} else {
				strncpy( key_uniq, key, sizeof(key_uniq) );
			}
			g_datalist_set_data( &klist, key, GUINT_TO_POINTER( count ) );
			const char* value = json_string_value( json_object_get( item, "value" ) );
			g_string_append_printf( Measurement, "%s=%s,", key_uniq, value );
		}
	}
	g_datalist_clear( &klist );

	json_t* host = json_object_get( json_line, "host" );
	char* host_name = normalize( json_string_value( json_object_get( host, "host" ) ) );
	g_string_append_printf( Measurement, "host=%s ", host_name );
	g_free( host_name );

	char* name = normalize( json_string_value( json_object_get( json_line, "name" ) ) );
	switch( json_integer_value(json_object_get( json_line, "type" )) ) {
		case 0:
			g_string_append_printf( Measurement, "%s=%.2f", name, json_number_value( json_object_get( json_line, "value" ) ) );
			break;
		case 1:
			g_string_append_printf( Measurement, "%s=\"%s\"", name, json_string_value( json_object_get( json_line, "value" ) ) );
			break;
		case 2:
			break;
		case 3:
			g_string_append_printf( Measurement, "%s=%lui", name, (unsigned long)json_integer_value( json_object_get( json_line, "value" ) ) );
			break;
		case 4:
			g_string_append_printf( Measurement, "%s=\"%s\"", name, json_string_value( json_object_get( json_line, "value" ) ) );
			break;
	}
	g_free( name );

	g_string_append_printf( Measurement, " %u\n", json_integer_value( json_object_get( json_line, "clock" ) ) );

	store_measure( Measurement );
}

static void process_json_line( json_t* json_line )
{
	json_t* type = json_object_get( json_line, "type" );
	if( json_integer_value(type) == 2 ) {
		// log
		return;
	}
	if( Verbose_flag > 3 ) {
		json_dumpf( json_line, stderr, JSON_INDENT(1) );
	}
	// Filter
	json_t* tags = json_object_get( json_line, "item_tags" );
	if( json_is_array( tags ) ) {
		for( int index = 0; index < json_array_size( tags ); index++ ) {
			json_t* item = json_array_get( tags, index );
			const char* key = json_string_value( json_object_get( item, "tag" ) );
			if( strcmp( key, "component" ) ) continue;	// next tag

			const char* value = json_string_value( json_object_get( item, "value" ) );
			struct _filter_s* p_filter = Filters;
			while( p_filter->component ) {
				if( !strcmp(  p_filter->component, value ) ) {
					decode_measurement( json_line, p_filter->measurement );
					return;
				}
				p_filter++;
			}
			// line not_filtered
			if( Reject_out ) {
				json_dumpf( json_line, Reject_out, JSON_INDENT(0) );
				fprintf( Reject_out, "\n=====\n" );
			}
			break;
		}
	}
}

static void server_callback( SoupServer *server,
							 SoupMessage *msg,
							 const char        *path,
							 GHashTable        *query,
							 SoupClientContext *client,
							 gpointer           user_data )
{
	SoupMessageBody* body  = msg->request_body;

	const char* lines = body->data;
	char* line = (char *)lines;
	char* fin = NULL;
	while( (fin = strchr( line, '\n')) != NULL ) {
		json_error_t	error;
		json_t* json_line = json_loadb( line, (fin - line), 0, &error );
		if( json_line == NULL ) {
			g_printerr( "Error decoding JSON: %s <line:%d/col:%d/pos:%d>\n",
					error.text, error.line, error.column, error.position );
			line = ++fin;
		}
		process_json_line( json_line );
		json_decref( json_line );
		line = ++fin;
		if( *line == '\n' || *line == '\0' ) break;
		if( Stop_asked == TRUE ) break;
	}

	soup_message_set_status( msg, SOUP_STATUS_OK );
}

static int connect_to_influx( json_t* config_influx )
{
	json_t* url = json_object_get( config_influx, "url" );
	if( url == NULL ) {
		fprintf( stderr, "Pas d'urL de connexion à influxDB. Ex http://localhost:8086.\n" );
		return 1;
	}
	json_t* base_json = json_object_get( config_influx, "base" );
	if( base_json == NULL ) {
		fprintf( stderr, "Pas de base influxDB spécifiée. Ex http://localhost:8086.\n" );
		return 1;
	}
	json_t* user_json = json_object_get( config_influx, "user" );
	json_t* pass_json = json_object_get( config_influx, "pass" );
	json_t* retention_json = json_object_get( config_influx, "retention" );

	Base = json_string_value( base_json );
	if( user_json ) {
		User = json_string_value( base_json );
		Pass = json_string_value( pass_json );
		if( User != NULL && Pass == NULL ) {
			fprintf( stderr, "Pas de mot de passe défini pour l'utilisateur \"%s\".\n", User );
		}
	}
	if( retention_json ) {
		Retention = json_string_value( retention_json );
	}
	GString* full_url = g_string_new(json_string_value(url));
	if( full_url->str[full_url->len] != '/' ) {
		g_string_append( full_url, "/" );
	}
	g_string_append_printf( full_url, "write?db=%s&precision=s&constitency=all", Base );
	if( User ) g_string_append_printf( full_url, "&u=%s&p=%s", User, Pass );
	if( Retention ) g_string_append_printf( full_url, "&rp=%s", Retention );

	if( Verbose_flag ) fprintf( stderr, "URL: %s\n",  full_url->str );

	if( curl_easy_setopt( Influx, CURLOPT_URL, full_url->str ) != CURLE_OK ) {
		fprintf( stderr, "URL: %s\n",  full_url->str );
		g_string_free( full_url, TRUE );
		return  1;
	}
	g_string_free( full_url, TRUE );
	return 0;
}

void stop( int sig )
{
	Stop_asked = TRUE;
	g_main_loop_quit( Loop );
}

int main( int argc, char* argv[] )
{
	static struct option long_options[] = {
		{"verbose", no_argument,       0, 'v'},
		{"version", no_argument,       0, 'V'},
		{"config",  required_argument, 0, 'c'},
		{0, 0, 0, 0}
	};
	char*	conffile = "zabbix_collect.conf";
	int		option_index = 0;

	int c = 0;
	while( 1 ) {
		c = getopt_long( argc, argv, "c:vV", long_options, &option_index );
		if( c == -1 ) break;
		switch( c ) {
			case 0:
				if( long_options[option_index].flag != 0 ) break;
				printf ("option %s", long_options[option_index].name);
				if( optarg  )
					printf( " with arg %s", optarg );
				printf ("\n");
				break;
			case 'c':
				conffile = optarg;
				break;
			case 'v':
				Verbose_flag++;
				break;
			case 'V':
				fprintf( stderr, "Version: %s\tBuild: %s.\n",
								ZBX_COLL_VERSION, ZBX_COLL_BUILD_TIME );
				exit( EXIT_SUCCESS );
			case '?':
				/* getopt_long already printed an error message. */
				exit( EXIT_FAILURE );
			default:
				exit( EXIT_FAILURE );
        }
    }
	if( Verbose_flag ) {
		fprintf( stderr, "Starting %s\n", argv[0] );
	}

	// Imports config.
	FILE* fin = fopen( conffile, "r" );
	if( fin == NULL ) {
		fprintf( stderr, "Cannot read configuration: " );
		perror( conffile );
		exit( EXIT_FAILURE );
	}

	if( Verbose_flag ) {
		fprintf( stderr, "Loading configuration %s\n", conffile );
	}
	json_error_t err_msg;
	json_t* config = json_loadf( fin, JSON_DECODE_ANY, &err_msg );

	if( config == NULL ) {
		fprintf( stderr, "erreur dans la configuration:\n%s ligne %d, colonne %d, (byte position %d)\n",
					err_msg.text, err_msg.line, err_msg.column, err_msg.position );
		exit( EXIT_FAILURE );
	}
	fclose( fin );

	if( Verbose_flag ) {
		fprintf( stderr, "CONFIGURATION:\n" );
		json_dumpf( config, stderr, JSON_INDENT(1) );
		fprintf( stderr, "=======\n" );
	}

	// Create filters
	json_t* filters = json_object_get( config, "filtres" );
	if( json_is_array( filters ) == FALSE ) {
		fprintf( stderr, "Erreur dans la configuration des filtres: obligatoire et doit être une array." );
		exit( EXIT_FAILURE );
	}
	size_t filters_len = json_array_size( filters );
	if( filters_len == 0 ) {
		fprintf( stderr, "Erreur dans la configuration des filtres: aucun filtre => pas de collecte." );
		exit( EXIT_FAILURE );
	}
	Filters = calloc( filters_len+1, sizeof(struct _filter_s) );
	if( !Filters ) {
		fprintf( stderr, "Erreur dans la configuration des filtres: allocation impossible." );
		exit( EXIT_FAILURE );
	}
	for( int i = 0; i < filters_len; i++ ) {
		json_t *item = json_array_get( filters, i );
		if( ! item ) continue;
		json_t* json_component = json_object_get( item, "component" );
		json_t* json_measurement = json_object_get( item, "measure" );
		if( json_component != NULL && json_measurement != NULL ) {
			Filters[i].component = json_string_value( json_component );
			Filters[i].measurement = json_string_value( json_measurement );
			if( Verbose_flag ) {
				fprintf( stderr, "Filters[%d] = { %s, %s }\n", i, Filters[i].component, Filters[i].measurement );
			}
			if( Filters[i].component == NULL || Filters[i].measurement == NULL ) {
				fprintf( stderr, "Erreur dans la configuration du filtre n° %d\n", i+1 );
				exit( EXIT_FAILURE );
			}
		}
	}
	// set reject
	Reject_out = NULL;
	json_t*	reject = json_object_get( config, "reject" );
	if( reject != NULL ) {
		json_t*	fichier = json_object_get( reject, "file" );
		if( fichier != NULL ) {
			Reject_out = fopen( json_string_value(fichier), "w" );
			if( Reject_out == NULL ) {
				fprintf( stderr, "Erreur lors de l'accès à " );
				perror( json_string_value(fichier) );
				exit( EXIT_FAILURE );
			}
		}
	}
	// set output
	json_t* output = json_object_get( config, "output" );
	if( output == NULL ) {
		fprintf( stderr, "Pas de configuration de sortie définie.\n" );
		exit( EXIT_FAILURE );
	}
	json_t* json_method = json_object_get( output, "method" );
	if( json_method == NULL ) {
		fprintf( stderr, "Pas de clé \"method\" : \"file\" ou \"influxdb\" définie.\n" );
		exit( EXIT_FAILURE );
	}
	const gchar* method = json_string_value( json_method );
	if( strcmp( method, "file" ) == 0 ) {
		Method_out = method_file;
		json_t* json_file = json_object_get( output, "file" );
		const gchar* fileout;
		if( json_file == NULL ) {
			fprintf( stderr, "Pas de clé \"file\" : \"file\" définie, utilisation de STDOUT.\n" );
			File_out = stdout;
		} else {
			fileout = json_string_value( json_file );
			File_out = fopen( fileout, "w" );
			if( File_out == NULL ) {
				fprintf( stderr, "Ouverture du fichier sortie impossible:" );
				perror( fileout );
				exit( EXIT_FAILURE );
			}
		}
	} else if( strcmp( method, "influx" ) == 0 ) {
		Method_out = method_influx;
		Influx = curl_easy_init();
		if( connect_to_influx( output ) != 0 ) {
			curl_easy_cleanup( Influx );
			exit( EXIT_FAILURE );
		}
		if( Verbose_flag ) {
			(void)curl_easy_setopt( Influx, CURLOPT_VERBOSE, 1L );
		}
		Lignes = g_list_prepend( Lignes, add_new_buffer_lignes());
	}
	// start http server (TODO HTTPS)
	gchar*	server_ecoute = NULL;
	guint	port_ecoute = 8080;
	json_t* json_input = json_object_get( config, "input" );
	json_t* json_server = json_object_get( json_input, "server" );
	json_t* json_port = json_object_get( json_input, "port" );
	if( json_input == NULL || json_port == NULL ) {
		fprintf( stderr, "Pas de configuration input { \"server\":\"localhost\", \"port\":8080 }" );
		fprintf( stderr, "Ecoute :8080" );
	}
	if( json_server ) server_ecoute = (gchar*)json_string_value( json_server );
	if( json_port ) port_ecoute = (guint)json_integer_value( json_port );

	GError* error = NULL;
	SoupServer* server = soup_server_new( "server-header", "zabbix_collect", NULL );
	if( server_ecoute == NULL ) {
		if( Verbose_flag ) fprintf( stderr, "HTTP server waiting on :%d ....\n", port_ecoute );
		if( soup_server_listen_all( server, port_ecoute, SOUP_SERVER_LISTEN_IPV4_ONLY, &error ) == FALSE ) {
			fprintf( stderr, "Listen error: %s\n", error->message );
			exit( EXIT_FAILURE );
		}
	} else {
		SoupAddress *soup_addr = soup_address_new( server_ecoute, port_ecoute );
		if( soup_address_resolve_sync( soup_addr, 0 ) != SOUP_STATUS_OK ) {
			fprintf( stderr, "Résolution de l'adresse %s impossible.\n", server_ecoute );
			exit( EXIT_FAILURE );
		}
		if( Verbose_flag ) fprintf( stderr, "HTTP server waiting on %s:%d ....\n", server_ecoute, port_ecoute );
		if( soup_server_listen( server, soup_address_get_gsockaddr( soup_addr ), 0, &error ) == FALSE ) {
			fprintf( stderr, "Listen error: %s\n", error->message );
			exit( EXIT_FAILURE );
		}
	}

	soup_server_add_handler( server, NULL, server_callback, NULL, NULL );
	// Wait for zabbix infos.
	Measurement = g_string_new( NULL );

	signal( SIGQUIT, stop );
	signal( SIGINT, stop );
	signal( SIGTERM, stop );

	Loop = g_main_loop_new( NULL, TRUE );
	g_main_loop_run( Loop );

	Stop_asked = FALSE;		// See send_to_influx()
	send_to_influx( TRUE );
	json_decref( config );
	if( Reject_out ) fclose( Reject_out );
	if( File_out )	fclose( File_out );
//	soup_server_disconnect( server );

	return EXIT_SUCCESS;
}
